@echo off
 :: BatchGotAdmin
 :-------------------------------------
 REM  --> Check for permissions
 >nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

REM --> If error flag set, we do not have admin.
 if '%errorlevel%' NEQ '0' (
     echo Requesting administrative privileges...
     goto UACPrompt
 ) else ( goto gotAdmin )

:UACPrompt
     echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
     echo UAC.ShellExecute "%~s0", "", "", "runas", 1 >> "%temp%\getadmin.vbs"

    "%temp%\getadmin.vbs"
     exit /B

:gotAdmin
     if exist "%temp%\getadmin.vbs" ( del "%temp%\getadmin.vbs" )
     pushd "%CD%"
     CD /D "%~dp0"
 :--------------------------------------

title Wonik IP Change

:message
cls
echo.
echo.
echo --------------------------
echo          IP  Change 
echo --------------------------
echo.
echo.
echo 1. Gumi IP
echo.
echo 2. Nanowin IP
echo.
echo 3. DHCP IP 
echo.
echo 4. IP Information
echo.
echo 5. exit
echo.
echo.
set /p select=Select Number...

if "%select%"=="1" goto gumi
if "%select%"=="2" goto nanowin
if "%select%"=="3" goto dhcp
if "%select%"=="4" goto ipconfig
if "%select%"=="5" goto end

goto message

:gumi
cls
echo.
echo.
echo ----------------------------- 
echo       Gumi IP
echo -----------------------------
echo.
echo.
netsh -c int ip set address name="Wi-Fi" static 10.0.30.213 255.255.254.0 10.0.30.1
netsh -c int ip set dns name="Wi-Fi" source=static addr=192.168.1.38 register=PRIMARY no
netsh -c int ip add dns name="Wi-Fi" addr = 192.168.1.39 index=2 no
pause
exit
; goto message

:nanowin
cls
echo.
echo.
echo ----------------------------- 
echo       Nanowin IP
echo -----------------------------
echo.
echo.
netsh -c int ip set address name="Wi-Fi" static 10.2.50.98 255.255.255.0 10.2.50.1
netsh -c int ip set dns name="Wi-Fi" source=static addr=164.124.101.2 register=PRIMARY no
netsh -c int ip add dns name="Wi-Fi" addr = 8.8.8.8 index=2 no
pause
exit
; goto message


:dhcp
cls
echo.
echo.
echo ------------------------------ 
echo          DHCP  IP
echo ------------------------------
echo.
echo.
netsh -c int ip set address "Wi-Fi" dhcp
netsh -c int ip set dns "Wi-Fi" dhcp
pause
exit
;goto message


:ipconfig
cls
echo.
echo.
echo ------------------------------ 
echo          IP Infomation
echo ------------------------------
ipconfig /all
pause
goto message

:end
exit
